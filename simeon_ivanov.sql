#FIRST ARTEZIO HOMEWORK - 'DATA BASE'

# TASK 1 -------------------------------------------------

CREATE DATABASE IF NOT EXISTS staff;

USE staff;

CREATE TABLE IF NOT EXISTS members ( #creating members' table
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(30) NOT NULL,
    last_name VARCHAR(30) NOT NULL,
    position_id INT NOT NULL,
    wage INT NOT NULL
);

CREATE TABLE IF NOT EXISTS positions ( #creating table with positions' names
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    position_name VARCHAR(30) NOT NULL
);

INSERT INTO positions #inserting positions' names
	(id, position_name)
VALUES
	(null, 'CEO'),
    (null, 'CMO'),
    (null, 'CFO'),
    (null, 'CHRO'),
    (null, 'Manager');
    
INSERT INTO members #inserting members' data
	(id, first_name, last_name, position_id, wage)
VALUES
	(null, 'Linus', 'Torvalds', 1, 1000000),
    (null, 'Elon', 'Musk', 2, 500000),
    (null, 'Steve', 'Jobs', 3, 400000),
    (null, 'Jeff', 'Bezos', 5, 20000),
    (null, 'Bill', 'Gates', 5, 110000),
    (null, 'Ilya', 'Kantor', 5, 10000);


# TASK 2 -------------------------------------------------

SELECT #all members with wages below 30 000
    members.first_name,
    members.last_name,
    positions.position_name,
    members.wage
FROM
    members
        INNER JOIN
    positions ON members.position_id = positions.id
WHERE
    wage < 30000;

SELECT #all Managers with wages below 30 000
    members.first_name,
    members.last_name,
    positions.position_name,
    members.wage
FROM
    members
        INNER JOIN
    positions ON members.position_id = positions.id
WHERE
    wage < 30000
        AND position_name = 'Manager';


# TASK 3 -------------------------------------------------

CREATE TABLE IF NOT EXISTS senior_junior ( #senior-to-junior relationships table
    senior_id INT,
    junior_id INT
);

INSERT INTO senior_junior #inserting senior-to-junior relationships
	(senior_id, junior_id)
VALUES
	(1, 2),
    (1, 3),
    (1, 4),
    (1, 5),
    (1, 6),
    (2, 4),
    (2, 5),
    (2, 6),
    (3, 4),
    (3, 5),
    (3, 6);


SELECT #selecting CEO's juniors (by First Name)
    CONCAT(senior.first_name, ' ', senior.last_name) AS SENIOR,
    CONCAT(junior.first_name, ' ', junior.last_name) AS JUNIOR
FROM
    senior_junior
        INNER JOIN
    members senior ON senior_junior.senior_id = senior.id
        INNER JOIN
    members junior ON senior_junior.junior_id = junior.id
        INNER JOIN
    positions ON senior.position_id = positions.id
WHERE
    junior.first_name = 'Linus';

    
SELECT #selecting Bill Gates' seniors
	CONCAT(junior.first_name, ' ', junior.last_name) AS JUNIOR,
    CONCAT(senior.first_name, ' ', senior.last_name) AS SENIOR
FROM
    senior_junior
		INNER JOIN
    members junior ON senior_junior.junior_id = junior.id
        INNER JOIN
    members senior ON senior_junior.senior_id = senior.id
WHERE
    junior.last_name = 'Gates';
